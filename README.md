# Ibexa Elasticsearch search engine Bundle

This package contains the Elasticsearch search engine implementation for [Ibexa OSS](https://ibexa.co).

## COPYRIGHT

Copyright (C) 1999-2023 Ibexa. All rights reserved.
